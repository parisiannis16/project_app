import React, { Component } from "react";
import RestaurantStore from "../stores/RestaurantStore";
import { Button, Modal } from "react-bootstrap";
import "../css/NavBarCustom.css";
import { sendRestaurant } from './UserFunctions'
import { deleteMock } from "./UserFunctions"
//You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports


export default class Admin extends Component {
    constructor() {
        super();
        this.storeMocks = new RestaurantStore();
        this.storeReviews = new RestaurantStore();
        this.state = {
            restaurants: [],
            reviews: [],
            show1: false,
            index: null
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.renderDetailsModal = this.renderDetailsModal.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
    }

    componentDidMount() {
        this.storeReviews.getReviews();
        this.storeMocks.getMock();
        this.storeMocks.emitter.addListener("GET_ALL_SUCCESS", () => {
            this.setState({
                restaurants: this.storeMocks.content
            });
        });
        this.storeReviews.emitter.addListener("GET_ALL_SUCCESS", () => {
            this.setState({
                reviews: this.storeReviews.content
            });
        });
    }

    onSubmit(vname, vprice, vwebsite, vimage, vlatitude, vlongitude, vuser_fk) {
        const restaurant = {
            name: vname,
            price: vprice,
            website: vwebsite,
            image: vimage,
            latitude: vlatitude,
            longitude: vlongitude,
            user_fk: vuser_fk,
        }

        sendRestaurant(restaurant).then(res => {
            // this.props.history.push(`http://localhost:3006/`)
            console.log("S-a rulat functia de trimitere restaurant, verifica baza de date!!!" + res)
        })
        deleteMock(restaurant).then(res => {
            console.log("Am sters mockul! pt " + restaurant.name);
        });
        window.location.href = "/admin"
    }

    onDelete(vname) {
        const restaurant = {
            name: vname,
        }
        deleteMock(restaurant).then(res => {
            console.log("Am sters mockul! pt " + restaurant.name);
        });
        window.location.href = "/admin"
    }

    handleClose() {
        this.setState({ show1: false });
    }

    renderDetailsModal(restaurant) {
        let link = "";
        if (restaurant.image !== null && restaurant.image !== undefined && restaurant.image.length > 0) {
            link = restaurant.image;
        }
        return (
            <Modal show={this.state.show1} onHide={this.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{restaurant.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="row-modal">
                            <div className="col-md-6 mt-5 mx-auto">
                                <form noValidate onSubmit={this.onSubmit}>
                                    <h1 className="h3 mb-3 font-weight-normal">Details</h1>
                                    <div className="form-group">
                                        <label htmlFor="name">Price</label>
                                        <input type="name"
                                            className="form-control"
                                            name="price"
                                            placeholder={restaurant.price}
                                            value={restaurant.price}
                                            readOnly
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="name">Website</label>
                                        <input type="name"
                                            className="form-control"
                                            name="website"
                                            placeholder={restaurant.website}
                                            value={restaurant.website}
                                            readOnly
                                        />
                                    </div>
                                    <div className="form-group">
                                        <img src={link} alt="Restaurant image" width="200" height="200" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="name">Latitude</label>
                                        <input type="name"
                                            className="form-control"
                                            name="latitude"
                                            placeholder={restaurant.latitude}
                                            value={restaurant.latitude}
                                            readOnly
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="name">Longitude</label>
                                        <input type="name"
                                            className="form-control"
                                            name="longitude"
                                            placeholder={restaurant.longitude}
                                            value={restaurant.longitude}
                                            readOnly
                                        />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <span>Paris Iannis 2019 </span>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Close
                          </Button>
                </Modal.Footer>
            </Modal>
        )
    }

    handleShow() {
        if (this.state.index !== null) {
            let restaurant = this.state.restaurants[this.state.index];
            return this.state.show1 ? this.renderDetailsModal(restaurant) : null;
        }
    }

    render() {
        //the welcome of the user jsx element
        //in render you delcare jsx element above render you declare functions!
        let restaurants = [];

        for (let i = 0; i < this.state.restaurants.length; i++) {
            restaurants.push(this.state.restaurants[i]);
        }

        let allMocks = restaurants.map((item, i) =>
            <li key={i} onClick={() => {
                this.setState({
                    show1: true,
                    index: i
                })

            }} className="list-group-item list-group-item li-ef list-group-item-info">
                <h4 className="list-group-item-heading ">{item.name}</h4>
                <h5 className="list-group-item-text">Added by {item.user_fk}</h5>
                <Button className="btn-success" onClick={() => {
                    this.onSubmit(item.name, item.price, item.website, item.image, item.latitude, item.longitude, item.user_fk)
                    alert("Ai ales sa adaugi cererea pentru " + item.name)
                }}>Accept</Button>
                <Button className="btn-danger" onClick={() => {
                    this.onDelete(item.name)
                    alert("Ai ales sa stergi cererea pentru " + item.name)
                }}>Deny</Button>
                <Button className="btn-success" onClick={() => {
                    this.props.sendData(item.name);
                    alert("Preview pentru " + item.name)
                }}>Preview</Button></li>)

        return (
            <div>
                <div className="list-group">
                    <ul className="ordered_list">
                        {allMocks}
                    </ul>
                </div>

                {this.handleShow()}
            </div>
        );
    }
}
