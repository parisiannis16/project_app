import React from "react";
import { Button, Modal } from "react-bootstrap";
import { sendRestaurantMock, getUser } from './UserFunctions'

export default class AddRestaurantEvent extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            show: true,
            name: '',
            price: '',
            website: '',
            image: '',
            latitude: this.props.handleLat,
            longitude: this.props.handleLong,
            user_fk: '',
        };
        this.handleClose = this.handleClose.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleCloseWithRefresh = this.handleCloseWithRefresh.bind(this);
        this.renderRightClickEvent = this.renderRightClickEvent.bind(this);
    }

    handleCloseWithRefresh() {
        this.setState({ show: false });
        window.location.href = "/"
    }

    handleClose() {
        this.setState({ show: false });
    }

    componentDidMount() {
        let obiectToken = JSON.parse(localStorage.getItem('usertoken'));
        // console.log(obiect.name)
        if (obiectToken) {
            let concatenare = "'" + obiectToken + "'";
            getUser(concatenare).then(res => {
                this.setState({
                    user_fk: res.data.user.id
                })
            })
        }
    }

    //reporneste modala dupa ce a fost inchisa si forteaza o recitirea a statelului din infowindow care decide daca deschide sau nu modala
    componentWillReceiveProps() {
        this.setState({
            show: true
        });
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()

        const restaurant = {
            name: this.state.name,
            price: this.state.price,
            website: this.state.website,
            image: this.state.image,
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            user_fk: this.state.user_fk,
        }

        sendRestaurantMock(restaurant).then(res => {
            // this.props.history.push(`http://localhost:3006/`)
            console.log("S-a rulat functia de trimitere restaurant, verifica baza de date!!!" + res)
        })
    }

    renderRightClickEvent(user_fk) {
        if (user_fk !== undefined && user_fk !== "") {
            return (
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add restaurant</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="row-modal">
                                <div className="col-md-6 mt-5 mx-auto">
                                    <form noValidate onSubmit={this.onSubmit}>
                                        <h1 className="h3 mb-3 font-weight-normal">Please fulfill all the information below</h1>
                                        <div className="form-group">
                                            <label htmlFor="name">Restaurant name</label>
                                            <input type="name"
                                                className="form-control"
                                                name="name"
                                                placeholder="Enter a name (Ex: McDonalds Unirii)"
                                                value={this.state.name}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Price</label>
                                            <input type="name"
                                                className="form-control"
                                                name="price"
                                                placeholder="How expensive it is?(1-5)"
                                                value={this.state.price}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Website</label>
                                            <input type="name"
                                                className="form-control"
                                                name="website"
                                                placeholder="Link for the website of the restaurant"
                                                value={this.state.website}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Image</label>
                                            <input type="name"
                                                className="form-control"
                                                name="image"
                                                placeholder="Provide a link to a representative image"
                                                value={this.state.image}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Latitude</label>
                                            <input type="name"
                                                className="form-control"
                                                name="latitude"
                                                placeholder={this.state.latitude}
                                                value={this.state.latitude}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Longitude</label>
                                            <input type="name"
                                                className="form-control"
                                                name="longitude"
                                                placeholder={this.state.longitude}
                                                value={this.state.longitude}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <button type="submit" onClick={this.handleCloseWithRefresh}
                                            className="btn btn-lg btn-primary btn-block">
                                            Save
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <span>Paris Iannis 2019 </span>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                          </Button>
                    </Modal.Footer>
                </Modal>
            )
        } else {
            return alert("You must be logged in!")
        }
    }

    render() {
        let user_fk = this.state.user_fk;

        return (
            <>
                {this.renderRightClickEvent(user_fk)}
            </>
        );
    }
}