import React from "react";
import { Button, Modal } from "react-bootstrap";
import { sendReview } from './UserFunctions'

export default class AddReview extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            show: this.props.handleModal,
            rating: '',
            comment: '',
            userFk: '',
            userName: '',
            restaurantFk: ''
        };
        this.handleClose = this.handleClose.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleCloseWithRefresh = this.handleCloseWithRefresh.bind(this);
    }

    handleCloseWithRefresh() {
        this.setState({ show: false });
        window.location.href = "/"
    }

    handleClose() {
        this.setState({ show: false });
    }

    componentDidMount() {
        this.setState(
            {
                userFk: this.props.handleUserId,
                userName: this.props.handleUserName,
                restaurantFk: this.props.handleRestaurantFk
            });
    }

    //reporneste modala dupa ce a fost inchisa si forteaza o recitirea a statelului din infowindow care decide daca deschide sau nu modala
    componentWillReceiveProps() {
        this.setState({
            show: true
        });
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()

        const review = {
            rating: this.state.rating,
            comment: this.state.comment,
            userFk: this.state.userFk,
            restaurantFk: this.state.restaurantFk
        }

        sendReview(review).then(res => {
            // this.props.history.push(`http://localhost:3006/`)
            console.log("S-a rulat functia de trimitere review, verifica baza de date!!!" + res)
        })
    }

    render() {
        return (
            <>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add review</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="row-modal">
                                <div className="col-md-6 mt-5 mx-auto">
                                    <form noValidate onSubmit={this.onSubmit}>
                                        <h1 className="h3 mb-3 font-weight-normal">Please fulfill all the information below</h1>
                                        <div className="form-group">
                                            <label htmlFor="name">Rating</label>
                                            <input type="name"
                                                className="form-control"
                                                name="rating"
                                                placeholder="Enter a rating from 1 to 5 (5 being the best rating)"
                                                value={this.state.rating}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Comment</label>
                                            <input type="name"
                                                className="form-control"
                                                name="comment"
                                                placeholder="Tell us something about your experience there!"
                                                value={this.state.comment}
                                                onChange={this.onChange}
                                            />
                                        </div>
                                        <button type="submit" onClick={this.handleCloseWithRefresh}
                                            className="btn btn-lg btn-primary btn-block">
                                            Save
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <span>Paris Iannis 2019 </span>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                          </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}