import React from "react";
import { Button, Modal } from "react-bootstrap";
import RestaurantStore from "../stores/RestaurantStore";

export default class Favorites extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.storeFavorite = new RestaurantStore();
        this.state = {
            show: this.props.handleFavoriteModal,
            favorites: [],
            id: ''
        };
        this.handleClose = this.handleClose.bind(this);
        this.handleCloseWithRefresh = this.handleCloseWithRefresh.bind(this);
    }

    handleCloseWithRefresh() {
        this.setState({ show: false });
        window.location.href = "/"
    }

    handleClose() {
        this.setState({
            show: false
        });
    }

    componentDidMount() {
        this.storeFavorite.getFavorite();

        this.storeFavorite.emitter.addListener("GET_ALL_SUCCESS", () => {
            this.setState({
                favorites: this.storeFavorite.content,
            });
        });

        this.setState({
            id: this.props.handleUID
        });
    }

    //reporneste modala dupa ce a fost inchisa si forteaza o recitirea a statelului din infowindow care decide daca deschide sau nu modala
    componentWillReceiveProps() {
        this.setState({
            show: true
        });
    }

    render() {
        let favorites = [];

        for (let i = 0; i < this.state.favorites.length; i++) {
            if (this.state.id === this.state.favorites[i].user_fk) {
                favorites.push(this.state.favorites[i]);
            }
        }

        let favoriteListItems = favorites.map((item, i) => <li key={i}>{item.name}</li>)

        return (
            <>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Your favorite restaurants</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="row-modal">
                                <ul className="col-md-6 mt-5 mx-auto">
                                    {favoriteListItems}
                                </ul>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <span>Paris Iannis 2019 </span>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Close
                          </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}