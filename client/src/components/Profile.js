import React, { Component } from 'react'
import Button from '@material-ui/core/Button';
import AddRestaurant from "./AddRestaurant";
import Favorites from "./Favorites";
import { getUser } from "./UserFunctions"

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            name: '',
            email: '',
            isLoaded: false,
            launchModal: false,
            launchFavoriteModal: false,
        }
        this.checkUserId = this.checkUserId.bind(this);
        this.renderModal = this.renderModal.bind(this);
        this.renderFavorites = this.renderFavorites.bind(this);
    }

    componentWillMount() {
        //aici e vector de obiecte si are un element
        //undefined la afisare si in state
        let obiectToken = JSON.parse(localStorage.getItem('usertoken'));
        // console.log(obiect.name)
        if (obiectToken) {
            let concatenare = "'" + obiectToken + "'";
            getUser(concatenare).then(res => {
                this.setState({
                    name: res.data.user.name,
                    email: res.data.user.email,
                    id: res.data.user.id,
                    isLoaded: true
                })
            })
        }
    }

    //trimiti inapoi date din copil in parinte 
    componentDidMount() {
        if (this.state.isLoaded) {
            this.props.handleChange('userName', this.state.name)
            this.props.handleChange('email', this.state.email)
        }
    }

    renderModal(userId) {
        if (this.state.launchModal === true) {
            return <AddRestaurant handleUID={userId} handleModal={this.state.launchModal} />
        }
        return null;
    }

    renderFavorites(userId) {
        if (this.state.launchFavoriteModal === true) {
            return <Favorites handleUID={userId} handleFavoriteModal={this.state.launchFavoriteModal} />
        }
        return null;
    }

    checkUserId() {
        if (this.state.id === undefined) {
            return "N am primit userId esti in Profile.js"
        }
        return this.state.id;
    }

    render() {
        let userId = this.checkUserId();

        return (
            <div className="container">
                <div className="jumbotron mt-5">
                    <div className="col-sm-3 mx-auto">
                        <h1 className="text-center">PROFILE</h1>
                    </div>
                    <table className="table col-md-6 mx-auto">
                        <tbody>
                            <tr>
                                <td>User name</td>
                                <td>{this.state.name}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{this.state.email}</td>
                            </tr>
                        </tbody>
                    </table>
                    <Button size="large" color="primary" onClick={() => {
                        this.setState({ launchModal: true });
                    }}>
                        Add restaurant
                    </Button>

                    {this.renderModal(userId)}

                    <Button size="large" color="primary" onClick={() => {
                        this.setState({ launchFavoriteModal: true });
                    }}>
                        See favorite restaurants
                    </Button>

                    {this.renderFavorites(userId)}
                </div>
            </div>
        )
    }
}

export default Profile