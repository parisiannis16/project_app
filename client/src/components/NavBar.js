import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { Button, Nav, DropdownButton, MenuItem } from "react-bootstrap";
import "../css/NavBarCustom.css";

class NavBar extends Component {
    logOut(e) {
        e.preventDefault()
        localStorage.removeItem('usertoken')
        //this.props.history.push(`/`)
        window.location.href = "/"
    }

    render() {
        const loginRegLink = (
            <ul className="nav-log nav-margin">
                <li>
                    <Button>
                        <Link to="/login" className="nav-link">
                            Login
                        </Link>
                    </Button>
                </li>
                <li>
                    <Button className="btn-space">
                        <Link to="/register" className="nav-link">
                            Register
                        </Link>
                    </Button>
                </li>
            </ul>
        )
        const userLink = (
            <ul className="nav-log nav-margin">
                <li>
                    <Button>
                        <Link to="/profile" className="nav-link">
                            User
                        </Link>
                    </Button>
                </li>
                <li>
                    <Button className="btn-space">
                        <a href="" onClick={this.logOut.bind(this)} className="nav-link">
                            Logout
                        </a>
                    </Button>
                </li>
            </ul>
        )

        return (
            <div className="logInBox">
                {localStorage.usertoken ? userLink : loginRegLink}
                {/* <div style={{ marginLeft: 50 + '%' }}>HELLO!</div> */}
            </div>
        )
    }
}

export default withRouter(NavBar)