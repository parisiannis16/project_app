import React from 'react'
import { getUser } from './UserFunctions'
import "../css/NavBarCustom.css";

class CurrentUser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "Guest"
        }
    }

    componentWillMount() {
        //aici e vector de obiecte si are un element
        //undefined la afisare si in state
        let obiectToken = JSON.parse(localStorage.getItem('usertoken'));
        if (obiectToken) {
            //to do verifici aici usertoken si dai mai departe
            let concatenare = "'" + obiectToken + "'";
            getUser(concatenare).then(res => {
                console.log("User ok!" + res.data.user.name);
                this.setState({
                    name: res.data.user.name
                })
            })
        }
    }

    componentDidMount() {
        this.props.handleChange('userName', this.state.name)
    }

    render() {
        const Guest = (
            <div className="current-user">
                <span>Signed in as: <a href="/">{this.props.name}</a></span>
            </div>
        )
        const User = (
            <div className="current-user">
                <span>Signed in as: <a href="/profile">{this.props.name}</a></span>
            </div >
        )

        return (
            <>
                {localStorage.usertoken ? User : Guest}
            </>
        )
    }
}

export default CurrentUser