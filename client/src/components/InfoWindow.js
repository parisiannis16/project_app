import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddReview from './AddReview';
import Icon from '@material-ui/core/Icon';
import { sendFavorite, getUser } from './UserFunctions'


class InfoWindow extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props)
        //iei user ul ca sa stii cine adauga review
        //restaurantul pentru care se adauga il iei din App.js prin handler
        this.state = {
            id: undefined,
            name: undefined,
            restaurantFk: undefined,
            launchModal: false,
            listItems: []
        }
        this.checkImage = this.checkImage.bind(this);
        this.checkUserId = this.checkUserId.bind(this);
        this.checkUserName = this.checkUserName.bind(this);
        this.checkRestaurantFk = this.checkRestaurantFk.bind(this);
        this.renderModal = this.renderModal.bind(this);
        this.addToFavorite = this.addToFavorite.bind(this);
    }

    addToFavorite(userId, restaurantId) {
        if (typeof userId !== 'undefined') {
            const favorite = {
                userFk: userId,
                restaurantFk: restaurantId
            }

            sendFavorite(favorite).then(res => {
                // this.props.history.push(`http://localhost:3006/`)
                console.log("S-a rulat functia de trimitere favorite, verifica baza de date!!!" + res)
                alert(favorite)
            })
        } else {
            alert("You must be logged in!")
        }
    }

    checkImage() {
        if (this.props.handleImage.length === 0) {
            return "https://dreamspicybd.com/wp-content/uploads/2017/03/fast-food-pictures-2.jpg";
        }
        return this.props.handleImage;
    }

    checkUserId() {
        if (this.state.id === undefined) {
            return "N am primit userId esti in infowindow"
        }
        return this.state.id;
    }

    checkUserName() {
        if (this.state.name === undefined) {
            return "You must be logged in"
        }
        return this.state.name;
    }

    checkRestaurantFk() {
        if (this.state.restaurantFk === undefined) {
            return "InfoWindow- n am primit id restaurant din componenta APP"
        }
        return this.state.restaurantFk;
    }

    renderModal(userId, userName, restaurantFk) {
        if (typeof userId !== 'undefined') {
            if (this.state.launchModal === true) {
                return <AddReview handleModal={this.state.launchModal} handleUserId={userId} handleUserName={userName} handleRestaurantFk={restaurantFk} />
            }
            return null;
        }
    }

    // listItemsGenerator() {
    //     let reviewListItems = this.props.handleReview.map((item, i) => <li key={i}>{item}</li>)
    //     this.setState({
    //         listItems: reviewListItems,
    //     })
    // }

    componentWillMount() {
        //aici e vector de obiecte si are un element
        //undefined la afisare si in state
        let obiectToken = JSON.parse(localStorage.getItem('usertoken'));
        // console.log(obiect.name)
        if (obiectToken) {
            let concatenare = "'" + obiectToken + "'";
            getUser(concatenare).then(res => {
                console.log("Info windows user data ok!" + res.data.user.id + res.data.user.name);
                this.setState({
                    id: res.data.user.id,
                    name: res.data.user.name
                })
            })
        }
    }

    componentDidMount() {
        this.setState({
            restaurantFk: this.props.handleId
        })
    }

    render() {
        const card = {
            maxWidth: 345,
        };

        const media = {
            height: 140,
        };
        //todo limit amount of review to be displayed
        let reviewListItems = this.props.handleReview.map((item, i) => <li key={i}>{item}</li>)

        let userId = this.checkUserId();
        let userName = this.checkUserName();
        let restaurantFk = this.checkRestaurantFk();
        const storeImage = this.checkImage();
        return (
            <Card style={card}>
                <CardActionArea>
                    <CardMedia
                        style={media}
                        image={storeImage}
                        title="O poza sugestiva a restauratului"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h4">
                            {this.props.handleName}
                        </Typography>
                        <Typography gutterBottom variant="h5">
                            The restaurant  has the rating {this.props.handleRating}
                            <ul>
                                {reviewListItems}
                            </ul>
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="large" color="primary" onClick={() => {
                        if (typeof this.state.id !== 'undefined') {
                            this.setState({ launchModal: true });
                        }
                        else {
                            alert("Trebuie sa fii logat pentru acesta actiune!")
                        }
                    }}>
                        Add review
                    </Button>
                    <Button size="large" color="primary" href={this.props.handleWebsite} target="_blank">
                        Learn More
                    </Button>
                    {<span onClick={() => {
                        if (typeof this.state.id !== 'undefined') {
                            this.addToFavorite(userId, restaurantFk)
                        } else {
                            alert("Trebuie sa fii logat pentru a adauga la favorite!")
                        }
                    }}><Icon className="fas fa-heart" /></span>}
                </CardActions>
                {this.renderModal(userId, userName, restaurantFk)}
            </Card>
        );
    }
}

InfoWindow.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default InfoWindow
