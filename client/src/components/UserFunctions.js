import axios from 'axios'

const SERVER = "http://localhost:3000";

export const deleteMock = mock => {
    return axios.delete(`${SERVER}/mock/`, { data: mock }).then(res => {
        //restaurantFk is undefined here
        console.log("Deleted 1 mock" + res.name)
        // Observe the data keyword this time. Very important
        // payload is the request body
        // Do something
    })
}

export const sendFavorite = newFavorite => {
    return axios
        .post(`${SERVER}/favorite/`, {
            userFk: newFavorite.userFk,
            restaurantFk: newFavorite.restaurantFk
        })
        .then(res => {
            //restaurantFk is undefined here
            console.log("Favorite added" + res.userFk + " " + res.restaurantFk)
        })
}

export const sendRestaurantMock = newRestaurant => {
    return axios
        .post(`${SERVER}/mock/`, {
            name: newRestaurant.name,
            price: newRestaurant.price,
            website: newRestaurant.website,
            image: newRestaurant.image,
            latitude: newRestaurant.latitude,
            longitude: newRestaurant.longitude,
            user_fk: newRestaurant.user_fk
        })
        .then(res => {
            //restaurantFk is undefined here
            console.log("Restaurant added" + newRestaurant.name + " " + newRestaurant.price + " by user " + newRestaurant.user_fk)
        })
}

export const sendRestaurant = newRestaurant => {
    return axios
        .post(`${SERVER}/restaurant/`, {
            name: newRestaurant.name,
            price: newRestaurant.price,
            website: newRestaurant.website,
            image: newRestaurant.image,
            latitude: newRestaurant.latitude,
            longitude: newRestaurant.longitude,
            user_fk: newRestaurant.user_fk
        })
        .then(res => {
            //restaurantFk is undefined here
            console.log("Restaurant added" + newRestaurant.name + " " + newRestaurant.price + " by user " + newRestaurant.user_fk)
        })
}

export const sendReview = newReview => {
    return axios
        .post(`${SERVER}/reviews/`, {
            rating: newReview.rating,
            comment: newReview.comment,
            userFk: newReview.userFk,
            restaurantFk: newReview.restaurantFk
        })
        .then(res => {
            //restaurantFk is undefined here
            console.log("Review added" + newReview.rating + " " + newReview.comment + " " + newReview.userFk + " " + newReview.restaurantFk)
        })
}

export const register = newUser => {
    return axios
        .post(`${SERVER}/register/`, {
            name: newUser.name,
            email: newUser.email,
            password: newUser.password
        })
        .then(res => {
            console.log("Registered" + newUser.name + " " + newUser.email + " " + newUser.password)
        })
}

export const login = user => {
    return axios
        .post(`${SERVER}/login/`, {
            email: user.email,
            password: user.password
        })
        .then(res => {
            console.log(res)
            console.log("res.data LALA " + res.data)
            // if (res.data[0].name === undefined) {
            //     console.log("Not found!!!!")
            //     alert("Wrong user name or password!")
            localStorage.setItem('usertoken', JSON.stringify(res.data))
            return JSON.stringify(res.data)
        })
        .catch(err => {
            console.log(err)
        })
}

export const getUser = userToken => {
    return axios
        .get(`${SERVER}/protected/`, {
            token: userToken,
            headers: {
                Authorization: 'Bearer ' + userToken //the token is a variable which holds the token
            }
        })
        .then(res => {
            console.log("res.data aici esti in JWT raspuns " + res.data)
            // if (res.data[0].name === undefined) {
            //     console.log("Not found!!!!")
            //     alert("Wrong user name or password!")
            return res.data
        })
        .catch(err => {
            console.log(err)
            // alert("Invalid credentials!")
        })
}