import axios from "axios";
import { EventEmitter } from "fbemitter";

const SERVER = "http://localhost:3000";

class RestaurantStore {
  constructor() {
    this.content = [];
    this.emitter = new EventEmitter();
  }
  async getMock() {
    try {
      let response = await axios(`${SERVER}/mock`);
      this.content = response.data;
      this.emitter.emit("GET_ALL_SUCCESS");
    } catch (e) {
      console.warn(e);
      this.emitter.emit("GET_ALL_ERROR");
    }
  }
  async getFavorite() {
    try {
      let response = await axios(`${SERVER}/favorite`);
      this.content = response.data;
      this.emitter.emit("GET_ALL_SUCCESS");
    } catch (e) {
      console.warn(e);
      this.emitter.emit("GET_ALL_ERROR");
    }
  }
  async getRating() {
    try {
      let response = await axios(`${SERVER}/rating`);
      this.content = response.data;
      this.emitter.emit("GET_ALL_SUCCESS");
    } catch (e) {
      console.warn(e);
      this.emitter.emit("GET_ALL_ERROR");
    }
  }
  async getReviews() {
    try {
      let response = await axios(`${SERVER}/reviews`);
      this.content = response.data;
      this.emitter.emit("GET_ALL_SUCCESS");
    } catch (e) {
      console.warn(e);
      this.emitter.emit("GET_ALL_ERROR");
    }
  }
  async getAll() {
    try {
      let response = await axios(`${SERVER}/restaurant`);
      this.content = response.data;
      this.emitter.emit("GET_ALL_SUCCESS");
    } catch (e) {
      console.warn(e);
      this.emitter.emit("GET_ALL_ERROR");
    }
  }
  async addOne(student) {
    try {
      await axios.post(`${SERVER}/restaurant`, student);
      this.emitter.emit("ADD_SUCCESS");
      this.getAll();
    } catch (e) {
      console.warn(e);
      this.emitter.emit("ADD_ERROR");
    }
  }
}

export default RestaurantStore;
