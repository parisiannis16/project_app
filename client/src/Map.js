import React, { Component } from "react";
import AddRestaurantEvent from "./components/AddRestaurantEvent";
import ReactDOM from "react-dom";
require("dotenv").config();

class Map extends Component {
  constructor(props) {
    super(props);
    this.onScriptLoad = this.onScriptLoad.bind(this);
    this.renderModal = this.renderModal.bind(this);
  }

  renderModal(lat, lng, id) {
    return <AddRestaurantEvent handleLat={lat} handleLong={lng} handleUID={id} />
  }

  onScriptLoad() {
    const map = new window.google.maps.Map(
      document.getElementById(this.props.id),
      this.props.options
    );

    map.addListener('rightclick', e => {
      var lat = e.latLng.lat();
      var lng = e.latLng.lng();
      const addRestaurant = new window.google.maps.InfoWindow({
        content: '<div id="infoWindowR" />',
        position: { lat: e.latLng.lat(), lng: e.latLng.lng() }
      })

      // populate yor box / field with lat, lng
      //alert("Lat=" + lat + "; Lng=" + lng);
      addRestaurant.addListener('domready', e => {
        ReactDOM.render(<AddRestaurantEvent handleLat={lat} handleLong={lng} />, document.getElementById('infoWindowR'))
      })
      addRestaurant.open(map)
    })

    const infoWindow = new window.google.maps.InfoWindow();

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function (position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          // var zoom = 16;
          infoWindow.setPosition(pos);
          infoWindow.setContent("Location found.");
          infoWindow.open(map);
          map.setCenter(pos);
          // map.setZoom(zoom);
        },
        function () {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      handleLocationError(false, infoWindow, map.getCenter());
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(
        browserHasGeolocation
          ? "Error: The Geolocation service failed."
          : "Error: Your browser doesn't support geolocation."
      );
      infoWindow.open(map);
    }
    this.props.onMapLoad(map);
  }

  componentDidMount() {
    if (!window.google) {
      var s = document.createElement("script");
      s.type = "text/javascript";
      const API = process.env.REACT_APP_API_KEY;
      s.src = `https://maps.google.com/maps/api/js?key=${API}`;
      var x = document.getElementsByTagName("script")[0];
      x.parentNode.insertBefore(s, x);
      // Below is important.
      //We cannot access google.maps until it's finished loading
      // let obiectToken = JSON.parse(localStorage.getItem('usertoken'));
      // // console.log(obiect.name)
      // if (obiectToken) {
      //   let concatenare = "'" + obiectToken + "'";
      //   getUser(concatenare).then(res => {
      //     this.setState({
      //       id: res.data.user.id
      //     })
      //   })
      // }

      s.addEventListener("load", e => {
        this.onScriptLoad();
      });
    } else {
      this.onScriptLoad();
    }
  }



  render() {
    const styleMap = {
      width: "100%",
      height: "100%",
      position: "absolute"
    };
    return <div style={styleMap} id={this.props.id} />;
  }
}

export default Map;
