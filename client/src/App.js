import React, { Component } from "react";
import ReactDOM from "react-dom";

import Map from "./Map";
import RestaurantStore from "./stores/RestaurantStore";
import { Navbar, Button, Nav, DropdownButton, MenuItem } from "react-bootstrap";
import "./css/NavBarCustom.css";
import { BrowserRouter as Router, Route } from 'react-router-dom'
import NavBar from "./components/NavBar"
import Login from "./components/Login.js"
import Register from "./components/Register"
import Profile from "./components/Profile.js"
import { Link } from 'react-router-dom'
import CurrentUser from './components/CurrentUser'
import InfoWindow from "./components/InfoWindow"
import Admin from "./components/Admin";
import { getUser } from "./components/UserFunctions"
//You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports

class App extends Component {
  constructor() {
    super();
    this.store = new RestaurantStore();
    this.storeReviews = new RestaurantStore();
    this.storeRating = new RestaurantStore();
    this.storeMocks = new RestaurantStore();
    this.storeFavorite = new RestaurantStore();

    this.state = {
      titePrice: "Price",
      titleRating: "Rating",
      restaurants: [],
      favorites: [],
      mocks: [],
      price: "",
      rating: [],
      avgRating: [],
      reviews: [],
      checkAll: false,
      userName: "Guest",
      email: "",
      numeMock: "",
      id: "",
      search: "",
    };
    // this.reviews = [];
    this.add = restaurant => {
      this.store.addOne(restaurant);
    };

    this.checkIfLoggedIn = this.checkIfLoggedIn.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addMarker = this.addMarker.bind(this);
    this.createInfoWindow = this.createInfoWindow.bind(this);
    this.renderAdminPanel = this.renderAdminPanel.bind(this);
    this.renderProfilePanel = this.renderProfilePanel.bind(this);
    this.getData = this.getData.bind(this);
    this.onChangeSearch = this.onChangeSearch.bind(this);
    this.getRandomInt = this.getRandomInt.bind(this);
    this.pushRandomComments = this.pushRandomComments.bind(this);
  }

  componentDidMount() {
    this.store.getAll();
    this.storeReviews.getReviews();
    this.storeRating.getRating();
    this.storeMocks.getMock();
    this.storeFavorite.getFavorite();

    this.storeMocks.emitter.addListener("GET_ALL_SUCCESS", () => {
      this.setState({
        mocks: this.storeMocks.content
      });
    });

    this.store.emitter.addListener("GET_ALL_SUCCESS", () => {
      this.setState({
        restaurants: this.store.content
      });
    });
    this.storeReviews.emitter.addListener("GET_ALL_SUCCESS", () => {
      this.setState({
        reviews: this.storeReviews.content
      });
    });
    this.storeRating.emitter.addListener("GET_ALL_SUCCESS", () => {
      this.setState({
        avgRating: this.storeRating.content
      });
    });
    this.storeFavorite.emitter.addListener("GET_ALL_SUCCESS", () => {
      this.setState({
        favorites: this.storeFavorite.content,
      });
    });

    this.setState({ checkAll: false });

    let obiectToken = JSON.parse(localStorage.getItem('usertoken'));
    //daca se relogheaza se mai incarca aici ? si se schimba ce e in localStorage??
    if (obiectToken) {
      let concatenare = "'" + obiectToken + "'";
      getUser(concatenare).then(res => {
        console.log("User ok in appJS!" + res.data.user.name);
        this.setState({
          userName: res.data.user.name,
          id: res.data.user.id
        })
      })
    }
  }

  createInfoWindow(e, map, ceRatingAre, restaurantName, website, image, review, restaurantId, contor) {
    const infoWindow = new window.google.maps.InfoWindow({
      content: '<div id="infoWindow' + contor + '" />',
      position: { lat: e.latLng.lat(), lng: e.latLng.lng() }
    })
    console.log("info " + ceRatingAre + " " + restaurantName)
    infoWindow.addListener('domready', e => {
      ReactDOM.render(<InfoWindow handleName={restaurantName} handleRating={ceRatingAre} handleWebsite={website} handleImage={image} handleReview={review} handleId={restaurantId} />, document.getElementById('infoWindow' + contor))
    })
    infoWindow.open(map)
  }

  //change the welcome of the user
  checkIfLoggedIn() {
    // event.preventDefault();
    if (localStorage.usertoken) {
      this.setState({
        isRegistered: true
      })
    }
  }

  handleChange(input, e) {
    if (this.state.email === "") {
      this.setState({
        [input]: e
      })
    }

  }

  //primesti date din componenta copil in componenta parinte(numele restaurantului)
  getData(val) {
    this.setState({ numeMock: val })
  }

  //generate random value
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  //iti pune 3 chestii random neduplicate din primul vector
  pushRandomComments(vectorLegit, vectorPozitiv) {
    let indexes = [];
    let index = this.getRandomInt(vectorPozitiv.length);

    let i = 0;
    do {
      // code block to be executed
      if (indexes.length === 0) {
        indexes.push(index);
        i++;
      } else if (indexes.length === 1) {
        if (indexes[0] !== index) {
          indexes.push(index);
          i++;
        } else {
          index = this.getRandomInt(vectorPozitiv.length);
        }
      } else {
        if (indexes[0] !== index && indexes[1] !== index) {
          indexes.push(index);
          i++;
        } else {
          index = this.getRandomInt(vectorPozitiv.length);
        }
      }

    }
    while (i < 3);

    vectorLegit.push(vectorPozitiv[indexes[0]].comment);
    vectorLegit.push(vectorPozitiv[indexes[1]].comment);
    vectorLegit.push(vectorPozitiv[indexes[2]].comment);

  }

  addMarker(markerData, map, index, culoare) {
    var LatLng = new window.google.maps.LatLng(
      markerData.latitude,
      markerData.longitude
    );

    var marker = new window.google.maps.Marker({
      position: LatLng,
      map: map,
    });

    //logica de schimbare culoare cand e favorit

    let favorites = [];

    for (let i = 0; i < this.state.favorites.length; i++) {
      if (this.state.id === this.state.favorites[i].user_fk) {
        favorites.push(this.state.favorites[i]);
      }
    }

    for (let i = 0; i < favorites.length; i++) {
      if (markerData.name === favorites[i].name) {
        culoare = "pink";
      }
    }

    if (culoare === "red") {
      marker.setIcon("http://maps.google.com/mapfiles/ms/icons/red-dot.png");

    } else if (culoare === "yellow") {
      marker.setIcon("http://maps.google.com/mapfiles/ms/icons/yellow-dot.png");

    } else if (culoare === "blue") {
      marker.setIcon("http://maps.google.com/mapfiles/ms/icons/blue-dot.png");

    } else if (culoare === "pink") {
      marker.setIcon("http://maps.google.com/mapfiles/ms/icons/pink-dot.png");

    } else if (culoare === "search") {
      marker.setIcon("https://i.imgur.com/KUBXtOM.png");
    }
    else {
      marker.setIcon("http://maps.google.com/mapfiles/ms/icons/green-dot.png");
    }


    let ceRatingAre = "no ratings yet!";
    let review = [];
    let website = "http://localhost:3006/";
    console.log(this.state.reviews.length);
    console.log("cate avg ratinguri sunt " + this.state.avgRating.length);

    //adauga commentul
    //ar trb facut vector daca sunt mai multe reviewuri la un restaurant// acm e doar 1 
    //TO DO cand o sa fie multe review-uri use

    //logica prelucare review
    let reviewMock = [];
    for (let i = 0; i < this.state.reviews.length; i++) {
      if (markerData.id === this.state.reviews[i].restaurant_fk) {
        reviewMock.push(this.state.reviews[i]);//tot obiectul de review
      }
    }

    if (reviewMock.length > 0) {
      let reviewPozitive = [];
      let reviewNegative = [];
      let countPozitive = 0;
      let countNegative = 0;
      //numeri sa vezi cate sunt peste 3 si mai mici decat 3
      for (let i = 0; i < reviewMock.length; i++) {
        if (reviewMock[i].rating <= 3) {
          reviewNegative.push(reviewMock[i]);
          countNegative++;
        } else {
          reviewPozitive.push(reviewMock[i]);
          countPozitive++;
        }
      }



      //adauga 3 random daca sunt peste 3 negative sau adauga toate cate sunt daca dimensiunea e mai mica de 3
      if (countPozitive >= 3) {
        this.pushRandomComments(review, reviewPozitive);
      } else {
        for (let i = 0; i < reviewPozitive.length; i++) {
          review.push(reviewPozitive[i].comment);
        }
      }

      if (countNegative >= 3) {
        this.pushRandomComments(review, reviewNegative);
      } else {
        for (let i = 0; i < reviewNegative.length; i++) {
          review.push(reviewNegative[i].comment);
        }
      }

    }

    //todo push comments intro review vector

    //handle website image
    if (markerData.website !== undefined) {
      website = markerData.website;
    }
    console.log("cate reviewuri are " + review.length + "ce e in review uri " + review);

    //adauga avg ratingul
    for (let i = 0; i < this.state.avgRating.length; i++) {
      if (markerData.id === this.state.avgRating[i].restaurant_fk) {
        ceRatingAre = this.state.avgRating[i].rating;
      }
    }

    let restaurantId = markerData.id;

    marker.addListener('click', e => {
      this.createInfoWindow(e, map, ceRatingAre, markerData.name, website, markerData.image, review, restaurantId, index)
    })
  }

  renderAdminPanel() {
    if (this.state.userName === "admin") {
      return <Route exact path="/admin" component={() => <Admin sendData={this.getData} />} />
    }
  }

  renderProfilePanel() {
    if (this.state.userName !== "Guest") {
      return <Route exact path="/profile" component={() => <Profile handleChange={this.handleChange} />} />
    }
  }

  onChangeSearch(e) {
    this.setState({ [e.target.name]: e.target.value })
  }


  render() {
    //the welcome of the user jsx element
    //in render you delcare jsx element above render you declare functions!

    // if (this.state.checkAll) {
    //   this.setState({
    //     checkAll: false
    //   })
    // }

    return (
      <div>
        <Router>
          <div className="containerStyle">
            <Navbar className="navbar-default stick">
              <Navbar.Header>
                <Navbar.Brand>
                  <Link to="/" className="nav-link titleFont">
                    FoodFINDR
                </Link>
                </Navbar.Brand>
              </Navbar.Header>
              <Nav className="navbar-nav btn-group btn:first-child">
                <DropdownButton
                  id="input-dropdown-addon"
                  title={this.state.titePrice}
                  bsStyle="danger"
                >
                  <MenuItem
                    eventKey={3.1}
                    onClick={() => {
                      this.setState({ price: 2 });
                      this.setState({ checkAll: false });
                      this.setState({ titePrice: "Budget" })
                    }}
                  >
                    Budget
                </MenuItem>
                  <MenuItem
                    eventKey={3.2}
                    onClick={() => {
                      this.setState({ price: 3 });
                      this.setState({ checkAll: false });
                      this.setState({ titePrice: "Expensive" })
                    }}
                  >
                    Expensive
                </MenuItem>
                  <MenuItem
                    eventKey={3.3}
                    onClick={() => {
                      this.setState({ price: 4 });
                      this.setState({ checkAll: false });
                      this.setState({ titePrice: "Luxury" })
                    }}
                  >
                    Luxury
                </MenuItem>
                </DropdownButton>
                <DropdownButton
                  id="input-dropdown-addon"
                  title={this.state.titleRating}
                  bsStyle="warning"
                >
                  <MenuItem
                    eventKey={4.1}
                    onClick={() => {
                      this.setState({
                        rating: [3.9, 3.8, 3.7, 3.6, 3.5, 3.4, 3.3, 3.2, 3.1, 3]
                      });
                      this.setState({ checkAll: false });
                      this.setState({ titleRating: "Good" })
                    }}
                  >
                    Good
                </MenuItem>
                  <MenuItem
                    eventKey={4.2}
                    onClick={() => {
                      this.setState({
                        rating: [4.9, 4.8, 4.7, 4.6, 4.5, 4.4, 4.3, 4.2, 4.1, 4]
                      });
                      this.setState({ checkAll: false });
                      this.setState({ titleRating: "Very good" })
                    }}
                  >
                    Very good
                </MenuItem>
                  <MenuItem
                    eventKey={4.3}
                    onClick={() => {
                      this.setState({ rating: 5 });
                      this.setState({ checkAll: false });
                      this.setState({ titleRating: "Excelent" })
                    }}
                  >
                    Excellent
                </MenuItem>
                </DropdownButton>
                <Button
                  bsStyle="success"
                  onClick={() => {
                    this.setState({ checkAll: true });
                    this.setState({ titleRating: "Rating" });
                    this.setState({ titePrice: "Price" })
                    this.setState({ price: "" })
                    this.setState({ rating: [] })
                  }}
                >
                  Show all
              </Button>
              </Nav>
              <Navbar.Text>
                <CurrentUser name={this.state.userName} handleChange={this.handleChange} />
                {/* <input class="form-control" type="text" placeholder="Search" aria-label="Search"></input> */}
                <form className="form-inline">
                  <input className="form-control mr-sm-2" type="search" name="search" placeholder="Eg: Restaurant Odai" aria-label="Search" value={this.state.search} onChange={this.onChangeSearch} />
                  <Button className="btn btn-outline-success my-2 my-sm-0" onClick={() => {
                    this.setState({
                      checkAll: true,
                    })
                  }}>Search</Button>
                </form>
                <NavBar />
              </Navbar.Text>
            </Navbar>

            <div className="container">
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              {this.renderAdminPanel()}
              {this.renderProfilePanel()}

            </div>

            <Map
              id="myMap"
              options={{
                center: { lat: 44.439663, lng: 26.096306 },
                zoom: 12
              }}
              user={this.state.name}
              key={[this.state.price, this.state.rating, this.state.checkAll, this.state.numeMock]}
              onMapLoad={map => {

                if (this.state.rating) {
                  for (let i = 0; i < this.state.restaurants.length; i++) {
                    this.state.avgRating.map(data => {
                      if (data.restaurant_fk === this.state.restaurants[i].id) {
                        let clona = {
                          id: this.state.restaurants[i].id,
                          image: this.state.restaurants[i].image,
                          latitude: this.state.restaurants[i].latitude,
                          longitude: this.state.restaurants[i].longitude,
                          name: this.state.restaurants[i].name,
                          price: this.state.restaurants[i].price,
                          user_fk: this.state.restaurants[i].user_fk,
                          website: this.state.restaurants[i].website,
                          rating: data.rating
                        }
                        this.state.restaurants[i] = clona;
                      }
                    });
                    console.log(this.state.restaurants[i].rating + " RESTAURANTUL MODIF NR " + i)
                    //ai 3 vectori legi 2 dintre ei restaurante si avg rating fiindca restaurante n au avg rating, tu adaugi un nou atribut, dupa care combini alti 2 vectori si anume cel din state.rating si vectorul nou frumos updatat
                    for (let a = 0; a < this.state.restaurants.length; a++) {
                      if (this.state.rating.length > 1) {
                        for (let b = 0; b < this.state.rating.length; b++) {
                          if (this.state.restaurants[a].rating && this.state.restaurants[a].rating === this.state.rating[b]) {
                            this.addMarker(this.state.restaurants[a], map, a, "yellow");
                          }
                        }
                      } else {
                        if (this.state.restaurants[a].rating && this.state.restaurants[a].rating === this.state.rating) {
                          this.addMarker(this.state.restaurants[a], map, a, "yellow");
                        }
                      }
                    }
                  }
                }

                if (this.state.price === 2) {
                  for (let i = 0; i < this.state.restaurants.length; i++) {
                    if (this.state.restaurants[i].price === 2) {
                      this.addMarker(this.state.restaurants[i], map, i, "red");
                    }
                  }
                } else if (this.state.price === 3) {
                  for (let i = 0; i < this.state.restaurants.length; i++) {
                    if (this.state.restaurants[i].price === 3) {
                      this.addMarker(this.state.restaurants[i], map, i, "red");
                    }
                  }
                } else if (this.state.price === 4) {
                  for (let i = 0; i < this.state.restaurants.length; i++) {
                    if (this.state.restaurants[i].price === 4) {
                      this.addMarker(this.state.restaurants[i], map, i, "red");
                    }
                  }
                } else {
                  if (this.state.rating.length === 0) {
                    for (let i = 0; i < this.state.restaurants.length; i++) {
                      this.addMarker(this.state.restaurants[i], map, i, "");
                    }
                  }
                }

                //afisaeza mockul
                if (this.state.numeMock) {
                  for (let i = 0; i < this.state.mocks.length; i++) {
                    if (this.state.mocks[i].name === this.state.numeMock) {
                      let a = 999;
                      let clona = this.state.mocks[i];
                      clona.id = a;
                      this.addMarker(clona, map, a, "blue");
                      a++;
                    }
                  }
                }

                //afisezi favorite-ul
                if (this.state.numeFavorite) {
                  for (let i = 0; i < this.state.restaurants.length; i++) {
                    if (this.state.restaurants[i].name === this.state.numeFavorite) {
                      let a = 999;
                      let clona = this.state.mocks[i];
                      clona.id = a;
                      this.addMarker(clona, map, a, "blue");
                      a++;
                    }
                  }
                }

                //afiseaza tot din db
                if (this.state.checkAll === true) {
                  for (let i = 0; i < this.state.restaurants.length; i++) {
                    if (this.state.search !== "") {
                      if (this.state.restaurants[i].name === this.state.search) {
                        this.addMarker(this.state.restaurants[i], map, i, "search");
                      }
                    } else {
                      this.addMarker(this.state.restaurants[i], map, i, "");
                    }
                  }
                }
              }}
            />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
