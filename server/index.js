var crypto = require('crypto');
var uuid = require('uuid');
var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

require('events').EventEmitter.prototype._maxListeners = 100;

var con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'foodfinder'
})

var getRandomString = function (length) {
    return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length)
}

var sha512 = function (password, salt) {
    var hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value,

    }
}

function saltHashPassword(userPassword) {
    var salt = getRandomString(16);
    var passwordData = sha512(userPassword, salt);
    return passwordData;
}

function checkHashPassword(userPassword, salt) {
    var passwordData = sha512(userPassword, salt);
    return passwordData;
}

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res, next) => {
    console.log('Password: 123456');
    var encrypt = saltHashPassword("123456");
    console.log('Encrypt: ' + encrypt.passwordHash);
    console.log('Salt:' + encrypt.salt);
})

app.post('/register/', (req, res, next) => {
    var post_data = req.body;
    var uid = uuid.v4();
    var plaint_password = post_data.password;
    var hash_data = saltHashPassword(plaint_password);
    var password = hash_data.passwordHash;
    var salt = hash_data.salt;
    var name = post_data.name;
    var email = post_data.email;

    con.query('SELECT * FROM user where email=?', [email], function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length)
            res.json('User already exists!');
        else {
            con.query('INSERT INTO `User`(`unique_id`, `name`, `email`, `encrypted_password`, `salt`, `created_at`, `updated_at`) ' +
                'VALUES (?,?,?,?,?,NOW(),NOW())', [uid, name, email, password, salt], function (err, result, fields) {
                    con.on('error', function (err) {
                        console.log('MySQL ERROR', err);
                        res.json('Register error!');
                    })
                    res.json('Register success!' + result + " fields: " + fields);
                })
        }
    })
})

app.post('/login/', (req, res, next) => {
    var post_data = req.body;
    //Extract email and password form request
    var user_password = post_data.password;
    var email = post_data.email;

    con.query('SELECT * FROM user where email=?', [email], function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length) {
            var salt = result[0].salt; // Get salt of result if the account exists
            var encrypted_password = result[0].encrypted_password;
            var hashed_password = checkHashPassword(user_password, salt).passwordHash;
            if (encrypted_password === hashed_password) {
                const token = jwt.sign({ user: result[0] }, 'secret_key_goes_here');
                res.json(token);
                res.end(JSON.stringify(result[0]));// If password is true, return all info of user returneaza un array cu un element !
            }
            else
                res.end(JSON.stringify('Wrong password'));
        }
        else {
            res.json('User does not exist!')
        }
    })
})

app.get('/protected', ensureToken, function (req, res) {
    let token = req.token;
    token = token.slice(1, -1);
    jwt.verify(token, "secret_key_goes_here", function (err, data) {
        if (err) {
            res.sendStatus(403);
        } else {
            res.json({
                data
            });
        }
    });
});

function ensureToken(req, res, next) {
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }
}

//login android
app.post('/androidlogin/', (req, res, next) => {
    var post_data = req.body;
    //Extract email and password form request
    var user_password = post_data.password;
    var email = post_data.email;

    con.query('SELECT * FROM user where email=?', [email], function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length) {
            var salt = result[0].salt; // Get salt of result if the account exists
            var encrypted_password = result[0].encrypted_password;
            var hashed_password = checkHashPassword(user_password, salt).passwordHash;
            if (encrypted_password === hashed_password) {
                const token = jwt.sign({ user: result[0] }, 'secret_key_goes_here');
                // res.json(token);
                res.end(JSON.stringify(result[0]));// If password is true, return all info of user returneaza un array cu un element !
            }
            else {
                let userel = { name: "" }
                res.end(JSON.stringify(userel));
            }
        }
        else {
            let user = { name: "" }
            res.json(user)
        }
    })
})

app.get('/restaurant/', (req, res, next) => {
    con.query('SELECT * FROM restaurant', function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length) {
            res.end(JSON.stringify(result));// If password is true, return all info of user 
        }
        else {
            res.json('Restaurant does not exist!')
        }
    })
})

app.post('/restaurant/', (req, res, next) => {
    var post_data = req.body;
    var name = post_data.name;
    var price = post_data.price;
    var website = post_data.website;
    var image = post_data.image;
    var latitude = post_data.latitude;
    var longitude = post_data.longitude;
    var user_fk = post_data.user_fk;
    con.query('SELECT * FROM restaurant where name=?', [name], function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length)
            res.json('Restaurant already exists!');
        else {
            con.query('INSERT INTO `restaurant`(`name`, `price`, `website`, `image`, `latitude`, `longitude`, `user_fk`) ' +
                'VALUES (?,?,?,?,?,?,?)', [name, price, website, image, latitude, longitude, user_fk], function (err, result, fields) {
                    if (err) throw err;
                    console.log("1 record inserted into restaurant");
                    res.json('Restaurant added success!' + result);
                })
        }
    })
})

//Mock
app.delete('/mock/', (req, res, next) => {
    var post_data = req.body;
    var name = post_data.name;
    var sql = 'DELETE FROM restaurantmock WHERE name=?';
    con.query(sql, [name], function (err, result) {
        if (err) throw err;
        console.log("Number of records deleted: " + result.affectedRows);
    });
})

app.get('/mock/', (req, res, next) => {
    con.query('SELECT * FROM restaurantmock', function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR]', err);
        })
        if (result && result.length) {
            res.end(JSON.stringify(result));// If password is true, return all info of user 
        }
        else {
            res.json(result)
        }
    })
})

//pt useri
app.post('/mock/', (req, res, next) => {
    var post_data = req.body;
    var name = post_data.name;
    var price = post_data.price;
    var website = post_data.website;
    var image = post_data.image;
    var latitude = post_data.latitude;
    var longitude = post_data.longitude;
    var user_fk = post_data.user_fk;
    con.query('SELECT * FROM restaurant where name=?', [name], function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length)
            res.json('Restaurant already exists!');
        else {
            con.query('INSERT INTO `restaurantmock`(`name`, `price`, `website`, `image`, `latitude`, `longitude`, `user_fk`) ' +
                'VALUES (?,?,?,?,?,?,?)', [name, price, website, image, latitude, longitude, user_fk], function (err, result, fields) {
                    if (err) throw err;
                    console.log("1 record inserted into restaurantMock");
                    res.json('Restaurant added success!' + result);
                })
        }
    })
})

app.get('/rating/', (req, res, next) => {
    con.query('SELECT TRUNCATE(AVG(rating),1) as rating, restaurant_fk FROM reviews group by restaurant_fk', function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length) {
            res.json(result);
            res.end(JSON.stringify(result));// If password is true, return all info of user 
        }
        else {
            res.json('Rating does not exist!')
        }
    })
})

app.get('/reviews/', (req, res, next) => {
    con.query('SELECT comment,user_fk,restaurant_fk,rating FROM reviews', function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length) {
            res.json(result);
            res.end(JSON.stringify(result));// If password is true, return all info of user 
        }
        else {
            res.json('Review does not exist!')
        }
    })
})

//adauga review merge doar daca adaugi de pe site nu poti testa cu postman user_fk cannot be null??!?
app.post('/reviews/', (req, res, next) => {
    var post_data = req.body;
    var rating = post_data.rating;
    var comment = post_data.comment;
    var userFk = post_data.userFk;
    var restaurantFk = post_data.restaurantFk;
    var sql = "INSERT INTO reviews(rating, comment, user_fk, restaurant_fk) VALUES (?,?,?,?)";
    con.query(sql, [rating, comment, userFk, restaurantFk], function (err, result) {
        if (err) throw err;
        console.log("1 record inserted");
        res.json('Review added success! ' + result);
    });
})

//custom post for android
app.post('/androidReviews/', (req, res, next) => {
    var post_data = req.body;
    var rating = parseFloat(post_data.rating);
    var comment = post_data.comment;
    var userFk = parseInt(post_data.userFk);
    var restaurantFk = parseInt(post_data.restaurantFk);


    var sql = "INSERT INTO reviews(rating, comment, user_fk, restaurant_fk) VALUES (?,?,?,?)";
    var sql2 = "SELECT comment,user_fk,restaurant_fk,rating FROM reviews";
    let counter = 0;

    //verifici daca a mai adaugat un review restaurantului
    con.query(sql2, function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length) {
            //aici se returneaza tot
            //verifici daca user-ul a mai adauga un review acestui restaurant
            for (let i = 0; i < result.length; i++) {
                if (result[i].restaurant_fk === restaurantFk && result[i].user_fk === userFk) {
                    counter++;
                }
            }
            if (counter == 0) {
                con.query(sql, [rating, comment, userFk, restaurantFk], (err, result) => {
                    if (err) throw err;
                    console.log("1 record inserted");
                    res.end('Added');
                    console.log(counter + " adaugat")
                });
            } else if (counter > 0) {
                res.end('You already added');
                console.log(counter + " n am adaugat")
            } else {
                res.end('idk');
                console.log(counter + " idk")
            }
        }
    })
})

//favorite table
app.get('/favorite/', (req, res, next) => {
    con.query('SELECT * FROM restaurant r, favorite f WHERE f.restaurant_fk= r.id', function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR]', err);
        })
        if (result && result.length) {
            res.json(result);
            res.end(JSON.stringify(result));// If password is true, return all info of user 
        }
        else {
            res.json('Favorite does not exist!')
        }
    })
})

// app.post('/favorite/', (req, res, next) => {
//     var post_data = req.body;
//     var userFk = post_data.userFk;
//     var restaurantFk = post_data.restaurantFk;
//     var sql = "INSERT INTO favorite(user_fk, restaurant_fk) VALUES (?,?)";
//     con.query(sql, [userFk, restaurantFk], function (err, result) {
//         if (err) throw err;
//         console.log("1 record inserted in favorite");
//         res.json('Favorite added success! ' + result);
//     });
// })

//un restaurant poate fi adaugat doar o data in favorite de catre un utilizator el trb sa fie unic adaugat per user
app.post('/favorite/', (req, res, next) => {
    var post_data = req.body;
    var userFk = post_data.userFk;
    var restaurantFk = post_data.restaurantFk;
    con.query('SELECT * FROM favorite where restaurant_fk=? AND user_fk=?', [restaurantFk, userFk], function (err, result, fields) {
        con.on('error', function (err) {
            console.log('[MySQL ERROR', err);
        })
        if (result && result.length)
            res.json('Restaurant already added to favorite!');
        else {
            var sql = "INSERT INTO favorite(user_fk, restaurant_fk) VALUES (?,?)";
            con.query(sql, [userFk, restaurantFk], function (err, result) {
                if (err) throw err;
                console.log("1 record inserted in favorite");
                res.json('Favorite added success! ' + result);
            });
        }
    })
})

app.listen(3000, () => {
    console.log("A pornit serveru.")
})